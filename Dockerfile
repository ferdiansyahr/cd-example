# Define base image
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env

# Copy project files
WORKDIR /source
COPY ["src/Example.Presentation/Example.Presentation.csproj", "./Example.Presentation/Example.Presentation.csproj"]

# Restore
RUN dotnet restore "./Example.Presentation/Example.Presentation.csproj"

# Copy all source code
COPY . .

# Publish
WORKDIR /source/src
RUN dotnet publish -c Release -o /publish

# Runtime
FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "Example.Presentation.dll"]
